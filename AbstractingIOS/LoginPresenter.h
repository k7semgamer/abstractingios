//
//  LoginPresenter.h
//  AbstractingIOS
//
//  Created by Kevin Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MVPLoginView.h"
#import "RegistrationColorThemeRepository.h"

@interface LoginPresenter : NSObject

- (instancetype) initWithView:(id<MVPLoginView>)loginView
                   repository:(id<RegistrationColorThemeRepository>)repository;

- (void) mvpViewDidLoad;

- (void) userDidSubmitUsername:(NSString *)username password:(NSString *)password;

@end
