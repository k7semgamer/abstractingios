//
//  LoginViewController.m
//  AbstractingIOS
//
//  Created by Kevin Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import "LoginViewController.h"
#import "ProfileViewController.h"

static NSString* const LoginViewControllerRegistrationColorSchemeKey;

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameUITextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordUITextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (strong, nonatomic) NSDictionary *registrationThemeDictionary;

@property (strong, nonatomic) NSURLSession *urlSession;
@property (strong, nonatomic) NSURLRequest *urlRequest;

@property (strong, nonatomic) NSUserDefaults *userDefaults;
@end

@implementation LoginViewController


- (void) viewDidLoad {
    
    self.registrationThemeDictionary = [self.userDefaults objectForKey:LoginViewControllerRegistrationColorSchemeKey];
    
    if (!self.registrationThemeDictionary) {
        
        [self.urlSession dataTaskWithRequest:self.urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            self.registrationThemeDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:nil];
            
        }];
        
    }
    
    //Sets button, dropdown, and background view colors
    [self configureViewWithThemeDictionary:self.registrationThemeDictionary];
}

- (IBAction)submitButtonTapped:(id)sender {
    
    BOOL usernameValid = [self checkUsernameValidity:self.usernameUITextField.text];
    
    BOOL passwordValid = [self checkPasswordValidity:self.passwordUITextField.text];
    
    if (!usernameValid || !passwordValid) {
        
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"You dummy"
                                                                                     message:@"Enter your shit right"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alertController animated:YES completion:nil];

            
            
        } else {
            
            [[[UIAlertView alloc] initWithTitle:@"You dummy"
                                       message:@"Enter your shit right"
                                      delegate:nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil] show];
            
        }
        
        
        
        
    } else {
        
        ProfileViewController *profileVC = [[ProfileViewController alloc] init];
        
        [self.navigationController pushViewController:profileVC animated:YES];
    }
    
}

- (void) configureViewWithThemeDictionary:(NSDictionary *)dictionary {
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    self.submitButton.tintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    self.usernameUITextField.tintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
}

- (BOOL) checkUsernameValidity:(NSString *)username {
    return NO;
}

- (BOOL) checkPasswordValidity:(NSString *)password {
    return NO;
}

@end