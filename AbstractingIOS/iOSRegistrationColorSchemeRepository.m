//
//  iOSRegistrationColorSchemeRepository.m
//  AbstractingIOS
//
//  Created by Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import "IOSRegistrationColorSchemeRepository.h"

static NSString* const RegistrationColorSchemeRepositoryKey;

@interface IOSRegistrationColorSchemeRepository ()

@property (strong, nonatomic) NSDictionary *registrationThemeDictionary;

@property (strong, nonatomic) NSURLSession *urlSession;
@property (strong, nonatomic) NSURLRequest *urlRequest;

@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation IOSRegistrationColorSchemeRepository

- (void) fetchColorThemeWithCompletion:(ColorSchemeFetchCompletionHandler)handler {
    
    self.registrationThemeDictionary = [self.userDefaults objectForKey:RegistrationColorSchemeRepositoryKey];
    
    if (!self.registrationThemeDictionary) {
        
        [self.urlSession dataTaskWithRequest:self.urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            self.registrationThemeDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:nil];
            
        }];
        
    }
    
    
}

@end
