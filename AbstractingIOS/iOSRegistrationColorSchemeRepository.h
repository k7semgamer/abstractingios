//
//  iOSRegistrationColorSchemeRepository.h
//  AbstractingIOS
//
//  Created by Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegistrationColorThemeRepository.h"

@interface IOSRegistrationColorSchemeRepository : NSObject <RegistrationColorThemeRepository>

@end
