//
//  MVPLoginView.h
//  AbstractingIOS
//
//  Created by Kevin Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MVPLoginView <NSObject>

- (void) showInvalidLoginCredsAlert;

- (void) setColorscheme:(NSDictionary *)colorschemeDictionary;

- (void) showLoggedInScreen;

@end
