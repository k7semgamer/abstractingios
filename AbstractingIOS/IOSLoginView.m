//
//  IOSLoginView.m
//  AbstractingIOS
//
//  Created by Kevin Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import "IOSLoginView.h"
#import "LoginPresenter.h"
#import "IOSRegistrationColorSchemeRepository.h"

@interface IOSLoginView ()

@property (strong, nonatomic) LoginPresenter *loginPresenter;

@end

@implementation IOSLoginView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loginPresenter mvpViewDidLoad];
}

- (void) showInvalidLoginCredsAlert {
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"You dummy"
                                                                                 message:@"Enter your shit right"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        
    } else {
        
        [[[UIAlertView alloc] initWithTitle:@"You dummy"
                                    message:@"Enter your shit right"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        
    }
    
}



- (LoginPresenter *)loginPresenter {
    
    if (!_loginPresenter) {
        
        IOSRegistrationColorSchemeRepository *colorSchemeRepository = [[IOSRegistrationColorSchemeRepository alloc] init];
        
        _loginPresenter = [[LoginPresenter alloc] initWithView:self repository:colorSchemeRepository];
        
    }
    
    return _loginPresenter;
}




@end
