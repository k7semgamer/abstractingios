//
//  LoginPresenter.m
//  AbstractingIOS
//
//  Created by Kevin Matthew Dupree on 3/3/15.
//  Copyright (c) 2015 Doctored Apps. All rights reserved.
//

#import "LoginPresenter.h"
@interface LoginPresenter ()
@property (strong, nonatomic, readonly) id<MVPLoginView> loginView;
@property (strong, nonatomic, readonly) id<RegistrationColorThemeRepository> repository;
@end

@implementation LoginPresenter
@synthesize loginView = _loginView;

- (instancetype) initWithView:(id<MVPLoginView>)loginView
                   repository:(id<RegistrationColorThemeRepository>)repository {
    
    if (self = [super init]) {
        _loginView = loginView;
        _repository = repository;
    }
    
    return self;
}

- (void) mvpViewDidLoad {
    
    [self.repository fetchColorThemeWithCompletion:^(NSDictionary *colorScheme) {
        
        [self.loginView setColorscheme:colorScheme];
    }];
    
}

- (void) userDidSubmitUsername:(NSString *)username
                      password:(NSString *)password {
    
    BOOL usernameValid = [self checkUsernameValidity:username];
    
    BOOL passwordValid = [self checkPasswordValidity:password];
    
    if (!usernameValid || !passwordValid) {
        
        [self.loginView showInvalidLoginCredsAlert];
        
    } else {
        
        [self.loginView showLoggedInScreen];
    }
    
}
    
- (BOOL) checkUsernameValidity:(NSString *)username {
    return NO;
}

- (BOOL) checkPasswordValidity:(NSString *)password {
    return NO;
}

@end
